## Potentail use cases

- Simulation of Evolutionary Biology theoretical models for evolution choices

## UI Ideas

- Render nodes on approprate level by counting number of levels of parent

## Refinements:

- Use Relationship property {crucial: true} instead of BLOCKS relationship

## Notes
- Raised Card property to show selected?
- Combine multiple queries and sort them by title then use indexes across them
- Sideways graph like in https://jpb12.github.io/tree-viewer/ with collapsable nodes
- use recursive flex lists. Show relations by just centering the root and recursing on that. 
- Probably a good idea to use Redux since updates will cause a cascade of changes
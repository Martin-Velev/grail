import ApolloClient from 'apollo-client'
import gql from 'graphql-tag'
import dotenv from 'dotenv'
import fetch from 'node-fetch'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import fs, { exists } from 'fs'
import path from 'path'
import neoDriver from '../neoDriver'

dotenv.config()

function seedFromGraphQL(file) {
	const seed = fs.readFileSync(path.join(__dirname, file)).toString('utf-8')
	const client = new ApolloClient({
		link: new HttpLink({
			uri: process.env.GRAPHQL_URI || 'http://localhost:4001/graphql',
			fetch,
		}),
		cache: new InMemoryCache(),
	})

	client
		.mutate({
			mutation: gql(seed),
		})
		.then(console.log)
		.catch(console.error)
}

async function seedFromCypher(file, constraintsFile = '') {
	const session = neoDriver.session()
	const seed = fs.readFileSync(path.join(__dirname, file)).toString('utf-8')
	const constraints = fs
		.readFileSync(path.join(__dirname, constraintsFile))
		.toString('utf-8')

	await session.run(constraints)
	await session.run(seed)
	session.close()
	console.log('Database seeded')
}

// seedFromGraphQL('seed.graphql')

seedFromCypher('seed.cypher', 'constraints.cypher')

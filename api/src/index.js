import express from 'express'
import dotenv from 'dotenv'
import { makeAugmentedSchema } from 'neo4j-graphql-js'
import { typeDefs } from './db/graphql-schema.js'
import { ApolloServer } from 'apollo-server-express'
import neoDriver from './neoDriver'
// SCHEMA: src/schema.graphql

// set environment variables from ../.env
dotenv.config()

const app = express()

const schema = makeAugmentedSchema({
	typeDefs,
})

/*
 * Create a new ApolloServer instance, serving the GraphQL schema
 * created using makeAugmentedSchema above and injecting the Neo4j driver
 * instance into the context object so it is available in the
 * generated resolvers to connect to the database.
 */
const server = new ApolloServer({
	context: { driver: neoDriver },
	schema,
	introspection: true,
	playground: true,
})
console.log(server.context)

const port = 4001
const path = '/graphql'

server.applyMiddleware({ app, path })

app.listen({ port, path }, () => {
	console.log(`GraphQL running on http://localhost:${port}${path}`)
})

import neo4j from 'neo4j-driver'

const neoDriver = neo4j.driver(
	'neo4j://localhost',
	neo4j.auth.basic('neo4j', 'eli')
)
export default neoDriver

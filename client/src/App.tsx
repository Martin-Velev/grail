import React from 'react'
import Root from './components/Root'
import './css/App.css'

const App = () => {
	return (
		<div className="App">
			<Root />
		</div>
	)
}

export default App

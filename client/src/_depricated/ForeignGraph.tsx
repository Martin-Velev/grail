import React, { SFC } from 'react'
import { GraphicalNode, GraphicalLink } from '../models'
import { Graph as D3Graph } from 'react-d3-graph'
import Loading from '../components/common/Loading'

type Props = {
	nodes: GraphicalNode[] | null
	links: GraphicalLink[] | null
}

const Graph: SFC<Props> = ({ nodes, links }) => {
	if (!nodes) return <Loading />
	return <D3Graph id="D3Graph" data={{ nodes, links }} />
}

export default Graph

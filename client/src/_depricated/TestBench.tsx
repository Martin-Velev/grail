import React, { SFC } from 'react'
import { GraphicalNode } from '../models'
import { Node } from 'react-d3-graph'

type Props = {
	nodes: GraphicalNode[] | null
}

const TestBench: SFC<Props> = ({ nodes }) => {
	console.log(nodes)
	return (
		<div id="TestBench">
			{nodes?.map(node => (
				<Node key={node.id} />
			))}
		</div>
	)
}

export default TestBench

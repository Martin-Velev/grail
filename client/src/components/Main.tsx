import React, { SFC } from 'react'
import { GoalModel } from '../models'
import '../css/Main.css'
import GoalLarge from './goal/GoalLarge'

type Props = {
	selectedGoal?: GoalModel | null
	refetch?: Function
}

const Main: SFC<Props> = ({ selectedGoal, refetch }) => {
	return (
		<div className="Main">
			{selectedGoal && (
				<GoalLarge goal={selectedGoal} refetch={refetch} />
			)}
		</div>
	)
}

export default Main

import React, { SFC } from 'react'
import '../css/Root.css'
import GoalsProvider from './wrappers/GoalsProvider'
import Main from './Main'
import GoalManager from './wrappers/GoalManager'
import Navigation from './navigation/Navigation'
import GoalFilter from './wrappers/GoalFilter'
import { GoalModel } from '../models'
import ToDoProvider from './wrappers/ToDoProvider'
import ToDoList from './todo/ToDoList'
import { hasTasks } from '../utils'
import GoalFattener from './wrappers/GoalFattener'

const Root: SFC<any> = () => {
	return (
		<div className="Root">
			<header className="App-header">
				<h1>Grail</h1>
			</header>
			<h1>Root</h1>
			<div className="Body">
				<GoalsProvider>
					<GoalFilter filter={(g: GoalModel) => !g.done}>
						<GoalManager>
							<Navigation />
							<Main />
						</GoalManager>
					</GoalFilter>
					<GoalFattener>
						<GoalFilter filter={(g: GoalModel, i, goals) => !hasTasks(g)}>
							<ToDoList />
						</GoalFilter>
					</GoalFattener>
				</GoalsProvider>
			</div>
		</div>
	)
}

export default Root

import React, { SFC } from 'react'

const Loading: SFC<any> = () => {
	return <div>Loading...</div>
}

export default Loading

import React, { SFC, ReactElement } from 'react'
import { renderChild } from '../../utils'

type Props = {
	children: ReactElement
}

const Tabs: SFC<Props> = ({ children, ...props }) => {
	return <div id="Tabs">{renderChild(children, props)}</div>
}

export default Tabs

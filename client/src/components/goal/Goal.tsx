import React, { useState, SFC, useEffect } from 'react'
import GoalCard from './GoalCard'
import { useQuery } from '@apollo/react-hooks'
import Loading from '../common/Loading'
import { GET_GOAL_BY_ID, GET_GOAL_BY_TITLE } from '../../queries.graphql'
import { GoalModel } from '../../models'

interface GoalProps {
	goalId?: string | null
	title?: string | null
}

const Goal: SFC<GoalProps> = ({ goalId, title }) => {
	const [goal, setGoal] = useState()
	const { loading, data, error } = useQuery(
		goalId ? GET_GOAL_BY_ID : GET_GOAL_BY_TITLE,
		{
			variables: {
				id: goalId,
				title: title,
			},
		}
	)
	useEffect(() => {
		if (!data || !data.Goal) return

		setGoal(data.Goal[0])
	}, [data])

	if (!goal || error) return null
	if (loading) return <Loading />
	return <GoalCard goal={goal} />
}

export default Goal

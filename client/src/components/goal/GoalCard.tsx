import React, { SFC } from 'react'
import { GoalModel } from '../../models'

import '../../css/GoalCard.css'

type Props = {
	goal: GoalModel
	level?: number
	selected?: boolean
}

const GoalCard: SFC<Props> = ({ goal, level = 0, selected = false }) => {
	const renderLevels = (levels: number) => {
		let bars = []

		for (let i = 0; i < levels; i++) {
			bars.push(<div key={i} className={'bar'}></div>)
		}
		return bars
	}

	const expandArrow = goal.expanded ? (
		<i className="expandArrow angle down icon"></i>
	) : (
		<i className="expandArrow angle right icon"></i>
	)

	const freespace = <div className="freeSpace"></div>

	return (
		<div id={goal.title} className={`GoalCard`}>
			<div className="levels">{renderLevels(level)}</div>
			<div className="treeGoal">
				<span>
					{goal.tasks && goal.tasks.length > 0 ? expandArrow : freespace}
				</span>

				<label className={`goalTitle ${selected ? 'selected' : ''}`}>
					{goal.title}
				</label>
			</div>
		</div>
	)
}

export default GoalCard

import React, { SFC, useState } from 'react'
import { GoalModel } from '../../models'
import '../../css/GoalLarge.css'
import { useMutation } from '@apollo/react-hooks'
import { DELTE_GOAL } from '../../mutations.grqphql'
import { trimString } from '../../utils'

type Props = {
	goal: GoalModel
	refetch?: Function
}

enum RelatedGoalTypes {
	Parent,
	Task,
}

const GoalLarge: SFC<Props> = ({ goal, refetch }) => {
	const [newParentLink, setNewParentLink] = useState()
	const [newTaskLink, setNewTaskLink] = useState()
	const [editMode, setEditMode] = useState(false)
	const [deleteGoal] = useMutation(DELTE_GOAL)
	if (!goal) return null

	const connectedGoals = (
		label: string,
		type: RelatedGoalTypes,
		relatedGoals: GoalModel[]
	) => {
		const getInputByType = (type: RelatedGoalTypes) => {
			switch (type) {
				case RelatedGoalTypes.Parent:
					return (
						<input
							onChange={(e: any) => setNewParentLink(e.target.value)}
							value={newParentLink}
							type="text"
							placeholder={label}
						/>
					)
				case RelatedGoalTypes.Task:
					return (
						<input
							onChange={(e: any) => setNewTaskLink(e.target.value)}
							value={newTaskLink}
							type="text"
							placeholder={label}
						/>
					)
			}
		}

		let input = getInputByType(type)
		return (
			<div>
				{label}
				{editMode && (
					<div className="row">
						<div className="ui right labeled left icon input form">
							<i className="tags icon"></i>
							{input}
						</div>
					</div>
				)}

				<div className="row overflow ">
					{relatedGoals?.map(parent => (
						<a className="item relatedGoalTag">
							<div className="ui horizontal label">
								{trimString(parent.title)}
							</div>
						</a>
					))}
				</div>
			</div>
		)
	}

	const buttons = [
		<button
			key="deleteButton"
			className="ui button red"
			onClick={() =>
				deleteGoal({ variables: { title: goal.title } }).then(
					() => refetch && refetch()
				)
			}>
			Delete
		</button>,
	]
	return (
		<div className="GoalLarge ui card">
			<div className="content">
				<div className="header">
					{goal.title}
					{!editMode && (
						<i
							onClick={() => setEditMode(true)}
							className="editButton pencil alternate icon"></i>
					)}
				</div>
			</div>
			<div className="content">
				{goal.desc && (
					<div>
						<h4 className="ui sub header">Description:</h4>
						<div className="content">
							<div className="summary">
								<p>{goal.desc}</p>
								<p></p>
							</div>
						</div>
					</div>
				)}
				<div className="col">
					{goal.parents
						? connectedGoals('parents', RelatedGoalTypes.Parent, goal.parents)
						: null}
					{goal.tasks
						? connectedGoals('tasks', RelatedGoalTypes.Task, goal.tasks)
						: null}
				</div>
			</div>
			<div className="extra content">{buttons}</div>
		</div>
	)
}

export default GoalLarge

import React, { SFC } from 'react'
import { GoalModel } from '../../models'
import GoalCard from './GoalCard'

type Props = {
	level?: number
	goal: GoalModel
	selectedGoalTitle?: string
	expanded?: boolean
	collapsedList?: string[]
}

const GoalWithChildren: SFC<Props> = ({
	selectedGoalTitle,
	goal,
	level = 0,
	expanded = true,
	collapsedList,
}) => {
	return (
		<React.Fragment>
			<GoalCard
				selected={selectedGoalTitle === goal.title}
				goal={{ ...goal, expanded: expanded }}
				level={level}
			/>
			{expanded &&
				goal.tasks?.map(g => (
					<GoalWithChildren
						collapsedList={collapsedList}
						expanded={collapsedList ? !collapsedList.includes(g.title) : true}
						selectedGoalTitle={selectedGoalTitle}
						key={g.title}
						goal={g}
						level={level + 1}
					/>
				))}
		</React.Fragment>
	)
}

export default GoalWithChildren

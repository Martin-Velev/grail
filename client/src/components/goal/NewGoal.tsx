import React, { SFC, useState, useEffect } from 'react'
import { useKeyPress } from '../../hooks'
import '../../css/NewGoal.css'

type Props = {
	onSubmit: Function
	cancel: Function
}

const NewGoal: SFC<Props> = ({ onSubmit, cancel }) => {
	const [goal, setGoal] = useState({ title: '' })
	const [active, setActive] = useState(true)

	useEffect(() => {
		document?.getElementById('goalInputField')?.focus()
	}, [])

	useKeyPress('Enter', () => onSubmit(goal))
	useKeyPress('Escape', cancel)

	return (
		<div id="newGoal">
			{/* <div className="levels">{renderLevels(level)}</div> */}
			<label htmlFor="title">New Goal: </label>
			<input
				id="goalInputField"
				value={goal.title}
				onChange={(e: any) => setGoal({ title: e.target.value })}
				type="text"
				placeholder="Title"
			/>
		</div>
	)
}

export default NewGoal

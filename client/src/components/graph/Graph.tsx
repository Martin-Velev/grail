import React, { SFC, ReactElement, useEffect, useState } from 'react'
import { GraphicalNode, GraphicalLink } from '../../models'
import '../../css/Graph.css'

import * as d3 from 'd3'
import { TAU, DEFAULT_R, DEFAULT_NODE_DISTANCE } from '../../constants'
console.log(d3.create('svg'))

type Props = {
	id: string
	w?: number
	h?: number
	nodes?: GraphicalNode[]
	links?: GraphicalLink[]
	children?: ReactElement
}

const Graph: SFC<Props> = ({
	nodes,
	links,
	children,
	id,
	w = '100%',
	h = '100%',
}) => {
	console.log('nodes', nodes)
	console.log('links', links)

	const [width, setWidth] = useState<number>(0)
	const [height, setHeight] = useState<number>(0)

	function renderGraph() {
		if (!nodes || !links) return

		const graphEl = document.getElementById('Graph')
		if (!graphEl) return
		const width = graphEl.clientWidth
		const height = graphEl.clientHeight

		const canvas = document.querySelector('canvas')
		if (!canvas) return
		const context = canvas.getContext('2d')
		if (!context) return
		const simulation = d3
			.forceSimulation()
			.force(
				'link',
				d3.forceLink().id((d: any) => d.id)
			)
			.force('charge', d3.forceManyBody())
			.force('center', d3.forceCenter())

		if (!simulation || !context) return

		const tick = () => {
			if (!context || !nodes || !links) return

			context.clearRect(0, 0, width, height)
			context.save()
			context.translate(width / 2, height / 2 + 40)

			context.beginPath()
			links.forEach(drawLink)
			context.strokeStyle = 'black'
			context.stroke()

			context.beginPath()
			nodes.forEach(drawNode)
			// context.fill()
			context.fillStyle = 'blue'
			context.strokeStyle = 'black'
			context.stroke()

			context.restore()
		}

		function drawLink(link: any) {
			if (!context) return
			const r = DEFAULT_R
			context?.moveTo(link.source.x - r, link.source.y)
			context?.lineTo(link.target.x - r, link.target.y)
		}
		function drawNode(node: any) {
			if (!context) return
			const r = DEFAULT_R
			context.moveTo(node.x + r, node.y)
			context.arc(node.x, node.y, r, 0, TAU)

			// Text
			context.font = '18px serif'
			context.fillText(node.id, node.x - r, node.y, r)
		}

		simulation
			.nodes(nodes)
			.on('tick', tick)
			.force(
				'collide',
				d3.forceCollide().radius(d => DEFAULT_R + DEFAULT_NODE_DISTANCE)
			)

		// if (!simulation.force('link')) return
		const linkForce: any = simulation.force('link')
		console.log('link force', linkForce)
		linkForce && linkForce.links(links)
		setWidth(width)
		setHeight(height)
		console.log('no set context?', context)
	}

	// renderGraph on startup
	useEffect(renderGraph, [])

	return (
		<div id="Graph">
			<canvas width={width} height={height}></canvas>

			{/* <div id={id} className="GraphSVGContainer">
				<svg
					name={`svg-container-${id}`}
					onClick={onClickGraph}
					style={{ width: w, height: h }}>
					{children && renderChild(children)}
					{nodes && nodes.map(node => <Node {...node} />)}
				</svg>
			</div> */}
		</div>
	)
}

export default Graph

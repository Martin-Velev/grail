import React, { SFC, useEffect } from 'react'
import NeoVis, { INeovisConfig } from 'neovis.js'
import '../../css/NeovizGraph.css'

const NeovizGraph: SFC = ({}) => {
	useEffect(() => {
		const config: INeovisConfig = {
			container_id: 'NeovizGraph',
			server_url: 'bolt://localhost:7687',
			server_user: 'neo4j',
			server_password: 'eli',
			arrows: true,
			labels: {
				Goal: {
					caption: 'title',
					// size: 'pagerank',
					// community: 'community',
				},
			},
			relationships: {
				CONTRIBUTES_TO: {
					caption: false,
					// thickness: 0.01,
				},
			},
			initial_cypher: 'MATCH (n)-[r:CONTRIBUTES_TO]-() RETURN n,r',
		}

    var viz = new NeoVis(config)
		viz.render()

		const conf = viz.registerOnEvent('clickNode', node => {
			console.log(node)
		})
	}, [])

	return <div id="NeovizGraph"></div>
}

export default NeovizGraph

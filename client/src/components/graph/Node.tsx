import React, { SFC, ReactElement } from 'react'
import { renderChild } from '../../utils'

type NodeSVG = {
	id: string
	x?: number
	y?: number
	children?: ReactElement
	w?: number
	h?: number
}

const Node: SFC<NodeSVG> = ({ children, x, y, w, h, id }) => {
	return (
		<g cx={x} cy={y} className="Node">
			{children ? (
				renderChild(children)
			) : (
				<React.Fragment>
					<path
						cursor="pointer"
						opacity="1"
						d="M7.978845608028654,0A7.978845608028654,7.978845608028654,0,1,1,-7.978845608028654,0A7.978845608028654,7.978845608028654,0,1,1,7.978845608028654,0"
						fill="#d3d3d3"
						stroke="none"
						stroke-width="1.5"></path>
					<text
						dx={x}
						dy={y}
						fill="black"
						font-size="8"
						font-weight="normal"
						opacity="1">
						{id}
					</text>
				</React.Fragment>
			)}
		</g>
	)
}

export default Node

import React, { SFC, useState, ReactElement } from 'react'
import Tree from './Tree'
import GraphConverter from '../wrappers/GraphConverter'
import Graph from '../graph/Graph'
import '../../css/Navigation.css'
import RelationProvider from '../wrappers/RelationProvider'
import NeovizGraph from '../graph/NeovizGraph'

type Tab = {
	name: string
	icon: string
	component: ReactElement
}

const TABS: Tab[] = [
	{
		name: 'Tree',
		icon: 'align left',
		component: <Tree />,
	},
	{
		name: 'NeovizGraph',
		icon: 'share alternate square ',
		component: <NeovizGraph />,
	},
	// {
	// 	name: 'D3Graph',
	// 	icon: 'share alternate square ',
	// 	component: (
	// 		<RelationProvider>
	// 			<GraphConverter>
	// 				<Graph id="navigationGraph" />
	// 			</GraphConverter>
	// 		</RelationProvider>
	// 	),
	// },
]

const Navigation: SFC = ({ ...props }) => {
	const [selectedTab, setSelectedTab] = useState<Tab>(TABS[0])

	// TODO: Disabled till you find a way to disable while editing
	// useKeyPress('t', () => selectTab('Tree'))
	// useKeyPress('g', () => selectTab('Graph'))

	return (
		<div className="Navigation">
			<div className="ui top attached tabular menu">
				{TABS.map(tab => (
					<a
						onClick={() => setSelectedTab(tab)}
						className={`item ${tab.name === selectedTab.name ? 'active' : ''}`}
						data-tab="first">
						<i className={tab.icon + ' icon'}></i>
					</a>
				))}
			</div>
			{React.cloneElement(selectedTab.component, props)}
		</div>
	)
}

export default Navigation

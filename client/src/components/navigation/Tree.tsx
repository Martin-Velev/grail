import '../../css/Tree.css'
import React, { SFC, useState, useEffect } from 'react'
import { GoalModel } from '../../models'
import GoalWithChildren from '../goal/GoalWithChildren'
import { useKeyPress } from '../../hooks'
import { flatten, computeScrollTarget, deepFilter } from '../../utils'
import { useMutation } from '@apollo/react-hooks'
import { ADD_TASK, DELTE_GOAL, MARK_DONE } from '../../mutations.grqphql'
import NewGoal from '../goal/NewGoal'

type Props = {
	goals?: GoalModel[]
	refetch?: Function
	selectedGoal?: GoalModel | null
	setSelectedGoal?: Function
}

const Tree: SFC<Props> = ({
	goals,
	refetch = () => {},
	selectedGoal,
	setSelectedGoal,
}) => {
	const [selectedIndex, setSelectedIndex] = useState<number>(0)
	const [editing, setEditing] = useState<boolean>(false)
	const [collapsedList, setCollapsedList] = useState<string[]>([])
	const [deleteGoal] = useMutation(DELTE_GOAL)
	const [addTask] = useMutation(ADD_TASK)
	const [markDone] = useMutation(MARK_DONE)
	const [flatGoals, setFlatGoals] = useState<GoalModel[]>(
		goals ? flatten(goals, collapsedList) : []
	)

	const down = () => {
		if (!flatGoals || flatGoals.length === 0) return
		if (selectedIndex === undefined || !setSelectedGoal) {
			setSelectedIndex(0)
		} else if (!editing && flatGoals && selectedIndex < flatGoals.length - 1) {
			setSelectedGoal(flatGoals[selectedIndex + 1])
		}
	}

	const up = () => {
		if (!flatGoals || flatGoals.length === 0) return
		if (selectedIndex === undefined || !setSelectedGoal) {
			setSelectedIndex(flatGoals.length - 1)
		} else if (!editing && selectedIndex > 0) {
			setSelectedGoal(flatGoals[selectedIndex - 1])
		} else return
	}

	const onAddTask = () => {
		if (!editing && flatGoals && selectedGoal) {
			setEditing(true)
		}
	}

	const onTaskSubmit = (g: GoalModel) => {
		setEditing(false)
		addTask({
			variables: { title: g.title, parent: selectedGoal?.title },
		}).then(() => {
			refetch()
		})
	}

	const selectClosestGoal = () => {
		if (setSelectedGoal) {
			let index = 0
			if (selectedIndex > 0) {
				index = selectedIndex - 1
			} else if (flatGoals.length > 1) {
				index = selectedIndex + 1
			}
			setSelectedGoal(flatGoals[index])
		}
	}

	const onDeleteGoal = () => {
		if (!editing && selectedGoal) {
			deleteGoal({ variables: { title: selectedGoal.title } }).then(() => {
				selectClosestGoal()
				refetch()
			})
		}
	}

	const collapse = () => {
		if (selectedGoal) {
			setCollapsedList((list: string[]) =>
				!list.includes(selectedGoal.title)
					? [...list, selectedGoal.title]
					: list
			)
		}
	}

	const expand = () => {
		setCollapsedList(list => list.filter(x => x !== selectedGoal?.title))
	}

	const onMarkDone = () => {
		if (!editing && selectedGoal) {
			markDone({
				variables: { title: selectedGoal.title, done: true },
			}).then(() => {
				selectClosestGoal()
				refetch()
			})
		}
	}

	useKeyPress('j', down, !editing)
	useKeyPress('k', up, !editing)
	useKeyPress('a', onAddTask, !editing)
	useKeyPress('x', onDeleteGoal, !editing)
	useKeyPress('h', collapse, !editing)
	useKeyPress('l', expand, !editing)
	useKeyPress('d', onMarkDone, !editing)

	useEffect(() => {
		if (goals) {
			setFlatGoals(flatten(goals, collapsedList))
		}
	}, [collapsedList, goals])

	// Select first goal
	useEffect(() => {
		goals && setSelectedGoal && setSelectedGoal(goals[0])
	}, [])

	// set index by selected goal
	useEffect(() => {
		if (selectedGoal && flatGoals) {
			const i: number = flatGoals.findIndex(
				(goal: GoalModel) => selectedGoal.title === goal.title
			)
			if (i !== undefined) {
				setSelectedIndex(i)

				// Scrolling
				const containerEl = document.getElementById('treeNav')
				const goalsAbove = flatGoals.slice(0, i).map(g => g.title)
				if (containerEl) {
					const scrollTarget = computeScrollTarget(
						goalsAbove,
						selectedGoal.title,
						'treeNav',
						document
					)
					if (scrollTarget !== -1) {
						containerEl.scrollTop = scrollTarget
					}
				}
			}
		}
	}, [setSelectedGoal, selectedGoal, flatGoals])

	return goals ? (
		<div id="treeNav" className="navigation">
			{editing && (
				<NewGoal onSubmit={onTaskSubmit} cancel={() => setEditing(false)} />
			)}
			{goals.map(g => (
				<GoalWithChildren
					collapsedList={collapsedList}
					expanded={!collapsedList.includes(g.title)}
					selectedGoalTitle={selectedGoal?.title}
					key={g.title}
					level={0}
					goal={g}
				/>
			))}
		</div>
	) : null
}

export default Tree

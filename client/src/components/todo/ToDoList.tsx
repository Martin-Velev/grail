import React, { SFC, useState, useEffect } from 'react'
import { GoalModel } from '../../models'
import Loading from '../common/Loading'
import '../../css/ToDoList.css'
import { DELTE_GOAL, DELETE_GOALS, MARK_DONE } from '../../mutations.grqphql'
import { useMutation } from '@apollo/react-hooks'

type Props = {
	goals?: GoalModel[]
	refetch?: Function
}

type ClearButtonProps = {
	doneGoals: GoalModel[]
	refetch: Function
}

const ClearAllButton: SFC<ClearButtonProps> = ({ doneGoals, refetch }) => {
	const [deleteDone] = useMutation(DELETE_GOALS(doneGoals))
	return (
		<i
			onClick={() => deleteDone().then(() => refetch && refetch())}
			className="trash icon clickable"></i>
	)
}

const ToDoList: SFC<Props> = ({ goals, refetch }) => {
	const [deleteGoal] = useMutation(DELTE_GOAL)
	const [markDone] = useMutation(MARK_DONE)
	const [doneGoals, setDoneGoals] = useState<GoalModel[] | null>(null)

	useEffect(updateDoneGoals, [goals])

	function updateDoneGoals() {
		const doneGoals = goals?.filter(g => g.done) || []
		if (doneGoals.length > 0) {
			setDoneGoals(doneGoals)
		}
	}

	function onCheckClicked(goal: GoalModel) {
		markDone({ variables: { title: goal.title, done: !goal.done } }).then(
			() => refetch && refetch()
		)
	}

	const toDoList = goals?.map(goal => (
		<div className="toDoItem">
			<div className="ui checkbox">
				<input
					onChange={() => onCheckClicked(goal)}
					type="checkbox"
					checked={goal.done == true} // explisit since null is considerred true for some fucking reason
				/>
				<label>{goal.title}</label>
			</div>
			<i
				onClick={() =>
					deleteGoal({ variables: { title: goal.title } }).then(
						() => refetch && refetch()
					)
				}
				className="toDoDeleteButton window close outline icon"></i>
		</div>
	))

	if (!goals) return <Loading />
	return (
		<div className="ToDoList">
			{doneGoals && doneGoals.length > 0 && refetch && (
				<ClearAllButton doneGoals={doneGoals} refetch={refetch} />
			)}
			<i
				onClick={() => refetch && refetch()}
				className="refresh icon clickable"></i>
			<ul>{toDoList}</ul>
		</div>
	)
}

export default ToDoList

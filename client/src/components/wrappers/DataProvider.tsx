import React, { SFC, ReactElement } from 'react'
import { useQuery, QueryHookOptions } from '@apollo/react-hooks'
import { DocumentNode } from 'graphql'
import Loading from '../common/Loading'

type Props = {
	query: DocumentNode
	children: ReactElement
	variables?: QueryHookOptions
	queryAccessor?: string
}

const DataProvider: SFC<Props> = ({ query, children, variables, queryAccessor }) => {
	const { data, loading, error } = useQuery(query, variables)

	if (loading) return <Loading />
	if (!data) return <div className="p">Fuck off</div>

	return (
		<div className="DataProvider">
			{React.cloneElement(children, { data: queryAccessor ? data[queryAccessor][0] : data })}
		</div>
	)
}

export default DataProvider

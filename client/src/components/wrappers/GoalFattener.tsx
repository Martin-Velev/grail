import React, { SFC, ReactElement } from 'react'
import { GoalModel } from '../../models'
import { renderChild, flatten } from '../../utils'

type Props = {
	children: ReactElement | ReactElement[]
	goals?: GoalModel[]
}

const GoalFattener: SFC<Props> = ({ goals, children, ...props }) => {
	return (
		<React.Fragment>
			{renderChild(children, {
				goals: goals && flatten(goals),
				...props,
			})}
		</React.Fragment>
	)
}

export default GoalFattener

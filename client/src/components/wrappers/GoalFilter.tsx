import React, { SFC, ReactElement } from 'react'
import { GoalModel } from '../../models'
import { renderChild, deepFilter, flatten } from '../../utils'

type Props = {
	children: ReactElement
	filter: (value: GoalModel, index: number, array: GoalModel[]) => boolean
	goals?: GoalModel[]
}

const GoalFilter: SFC<Props> = ({ children, goals, filter, ...props }) => {
	if (goals && filter) {
		console.log('filtered', deepFilter(goals, filter))
		console.log('from', goals)
	}
	return (
		<React.Fragment>
			{renderChild(children, {
				goals: goals ? deepFilter(goals, filter) : null,
				...props,
			})}
		</React.Fragment>
	)
}

export default GoalFilter

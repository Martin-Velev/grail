import React, { SFC, useState, ReactElement } from 'react'
import { renderChild } from '../../utils'
import { GoalModel } from '../../models'

type Props = {
	children: ReactElement[]
	goals?: GoalModel[]
	// ...props: any
}

const GoalManager: SFC<Props> = ({ children, goals, ...props }) => {
	const [selectedGoal, setSelectedGoal] = useState(null)

	return (
		<React.Fragment>
			{children.map(child =>
				renderChild(child, {
					goals,
					selectedGoal,
					setSelectedGoal,
					...props,
				})
			)}
		</React.Fragment>
	)
}

export default GoalManager

import React, { SFC, ReactElement } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { GRAIL } from '../../queries.graphql'
import Loading from '../common/Loading'
import ErrorScreen from '../common/ErrorScreen'
import { renderChild } from '../../utils'

type Props = {
	children: ReactElement | ReactElement[]
}

const GoalsProvider: SFC<Props> = ({ children, ...props }) => {
	const { data, loading, error, refetch } = useQuery(GRAIL(12))

	if (loading) return <Loading />
	if (error) return <ErrorScreen />
	return (
		<React.Fragment>
			{renderChild(children, {
        goals: data.grail,
        refetch,
				...props,
			})}
		</React.Fragment>
	)
}

export default GoalsProvider

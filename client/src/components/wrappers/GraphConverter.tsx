import React, { SFC, useState, useEffect, ReactElement } from 'react'
import Loading from '../common/Loading'
import {
	GoalModel,
	GraphicalNode,
	GraphicalLink,
	LinkModel,
} from '../../models'
import {
	goalToNode,
	renderChild,
	relationsToNodeLinks,
	deepMap,
	flatten,
} from '../../utils'

type Props = {
	children: ReactElement
	relations?: LinkModel[]
	goals?: GoalModel[]
}

const GraphConverter: SFC<Props> = ({
	children,
	goals,
	relations,
	...props
}) => {
	const [nodes, setNodes] = useState<GraphicalNode[]>()
	const [links, setLinks] = useState<GraphicalLink[]>()

	useEffect(() => {
		if (!goals) return
		const nodes: GraphicalNode[] = flatten(goals).map(goalToNode)
		const links: GraphicalLink[] = relationsToNodeLinks(relations || [])

		setNodes(nodes)
		setLinks(links)
	}, [goals])

	if (!children || !goals || !nodes) return <Loading />
	return (
		<React.Fragment>
			{renderChild(children, { nodes, links, ...props })}
		</React.Fragment>
	)
}

export default GraphConverter

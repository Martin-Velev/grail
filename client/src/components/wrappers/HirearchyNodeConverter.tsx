import React, { SFC, useEffect, ReactElement } from 'react'
import { GraphicalNode, GoalModel } from '../../models'
import { goalToNode } from '../../utils'
import Loading from '../common/Loading'
import * as d3 from 'd3'

type Props = {
	children: ReactElement
	root?: GoalModel
}

const HirearchyNodeConverter: SFC<Props> = ({ root, children }) => {
	// return <div>Fuck you</div>
	if (!root) return <Loading />
	return (
		<div id="HirearchyNodeConverter">
			{React.cloneElement(children, {
				root: d3.hierarchy(goalToNode(root)),
			})}
		</div>
	)
}

export default HirearchyNodeConverter

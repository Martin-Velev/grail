import React, { SFC, useEffect, ReactElement } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { GET_ROOT } from '../../queries.graphql'
import Loading from '../common/Loading'
import ErrorScreen from '../common/ErrorScreen'

type Props = {
	children: ReactElement
	root?: string
}

const HirearchyProvider: SFC<Props> = ({ children, root }) => {
	const { data, loading, error } = useQuery(GET_ROOT(-1), {
		variables: { rootTitle: root },
  })

	if (loading) return <Loading />
	if (error) return <ErrorScreen />
	return <div id="DataProvider">{React.cloneElement(children, { root: data.Goal[0] })}</div>
}

export default HirearchyProvider

import React, { SFC, ReactElement } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { GRAIL, GET_ALL_RELATIONS } from '../../queries.graphql'
import Loading from '../common/Loading'
import ErrorScreen from '../common/ErrorScreen'
import { renderChild } from '../../utils'

type Props = {
	children: ReactElement
}

const RelationProvider: SFC<Props> = ({ children, ...props }) => {
	const { data, loading, error, refetch } = useQuery(GET_ALL_RELATIONS)

	if (loading) return <Loading />
	if (error) return <ErrorScreen />
	return (
		<React.Fragment>
			{renderChild(children, { relations: data.links, ...props })}
		</React.Fragment>
	)
}

export default RelationProvider

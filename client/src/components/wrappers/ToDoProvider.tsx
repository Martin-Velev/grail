import React, { SFC, ReactElement } from 'react'
import { renderChild } from '../../utils'
import { GET_LEAFS } from '../../queries.graphql'
import { useQuery } from '@apollo/react-hooks'
import Loading from '../common/Loading'
import ErrorScreen from '../common/ErrorScreen'

type Props = {
	children: ReactElement
}

const ToDoProvider: SFC<Props> = ({ children, ...props }) => {
	const { data, loading, error, refetch } = useQuery(GET_LEAFS)

	if (loading) return <Loading />
	if (error) return <ErrorScreen />
	return (
		<React.Fragment>
			{renderChild(children, {
				goals: data.leafs,
				refetch,
				...props,
			})}
		</React.Fragment>
	)
}

export default ToDoProvider

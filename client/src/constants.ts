export const PI = Math.PI
export const TAU = 2 * PI

export const MAX_DEPTH = 12

export const MARGIN = 30

export const DEFAULT_R = 30
export const DEFAULT_NODE_DISTANCE = 20
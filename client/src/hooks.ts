import { useEffect, useState } from 'react'

export function useKeyPress(
	targetKey: string,
	onPress: Function,
	active: boolean = true
) {
	const [repeating, setRepeating] = useState(false)

	const stopRepeat = () => setRepeating(false)

	useEffect(() => {
		const downHandler = ({ key }: KeyboardEvent) =>
			key === targetKey && active ? onPress() : null

		window.addEventListener('keydown', downHandler)
		window.addEventListener('keyup', stopRepeat) // Stop repeat

		return () => {
			window.removeEventListener('keydown', downHandler)
			window.removeEventListener('keyup', stopRepeat)
		}
	}, [repeating, targetKey, onPress, active])
}

import { SimulationNodeDatum, SimulationLinkDatum } from 'd3'

export type LinkModel = {
	title: string
	link: { from: { Goal: GoalModel }[]; to: { Goal: GoalModel }[] } // TODO: there should be a cleaner way to do this
}

export type GoalModel = {
	title: string // ID!
	done?: boolean
	_id?: number
	tasks?: GoalModel[]
	parents?: GoalModel[]
	desc?: string
	link?: LinkModel
	hasChildren?: boolean
	editMode?: boolean
	expanded?: boolean
}

export type GoalMetaData = {
	title: string
}

export interface GraphicalNode extends SimulationNodeDatum {
	id: string
}

export interface GraphicalLink extends SimulationLinkDatum<GraphicalNode> {
	source: string
	target: string
}

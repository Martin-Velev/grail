import gql from 'graphql-tag'
import { GoalModel } from './models'

// Craetes a floating goal
export const CREATE_GOAL = gql`
	mutation($title: ID) {
		CreateGoal(title: $title) {
			title
		}
	}
`

export const ADD_TASK = gql`
	mutation($title: ID!, $parent: ID!) {
		child: CreateGoal(title: $title) {
			title
		}
		link: AddGoalTasks(from: { title: $title }, to: { title: $parent }) {
			from {
				title
			}
			to {
				title
			}
		}
	}
`

export const DELTE_GOAL = gql`
	mutation($title: ID!) {
		DeleteGoal(title: $title) {
			title
		}
	}
`

export const MERGE_GOAL = gql`
	mutation($title: ID!) {
		MergeGoal(title: $title) {
			title
		}
	}
`

export const MARK_DONE = gql`
	mutation($title: ID!, $done: Boolean) {
		MergeGoal(title: $title, done: $done) {
			title
			done
		}
		detach(title: $title) {
			title
		}
	}
`
export const DELETE_GOALS = (goals: GoalModel[]) => gql`
  mutation {
    ${goals.map(
      (g, i) => `		
      delete${i}: DeleteGoal(title: "${g.title}") {
        title
      }`
    )}
  }
`

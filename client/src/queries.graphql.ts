import gql from 'graphql-tag'
import { MAX_DEPTH } from './constants'

const children = (depth: number = 0): String => {
	if (depth === -1) return children(MAX_DEPTH)
	if (depth === 0) return ''
	return `
    tasks{
      title
      done
      parents {
        title
        done
      }
      ${children(depth - 1)}
    }
  `
}
/**
 * Query Funciton. Needs to be called GET_ROOT(:depth)
 * Fetches a graph of Goals from root and recurses down to given depth of tasks
 * @param depth (0)-only root (1)-Only immidiete sub-tasks (-1)-Max Depth
 */
export const GET_ROOT = (depth: number = 0) => gql`
	query root($rootTitle: ID) {
		Goal(title: $rootTitle, orderBy:title_asc) {
			title
      desc
      done
      ${children(depth)}
		}
	}
`

/**
 * Returns all nodes as a flat array
 */
export const GET_ALL_GOALS = gql`
	query getAllGoals {
		goals {
			title
			done
			desc
		}
	}
`

/**
 * Returns all nodes as a flat array with parent title
 */
export const GET_ALL_GOALS_WITH_PARENT = gql`
	query getAllGoals {
		goals {
			title
			done
			desc
		}
	}
`

export const GET_GOAL_BY_TITLE = gql`
	query($title: String) {
		Goal(title: $title) {
			title
			done
			desc
		}
	}
`

export const GET_GOAL_BY_ID = gql`
	query($id: ID) {
		Goal(id: $id) {
			title
			done
			desc
		}
	}
`

export const GET_ALL_WITH_LINKS = gql`
	query {
		goals {
			title
			done
			desc
			link {
				from {
					Goal {
						title
						done
					}
				}
				to {
					Goal {
						title
						done
					}
				}
			}
		}
	}
`

export const GET_LEAFS = gql`
	{
		leafs {
			title
			done
		}
	}
`

/**
 * Fetch the top most goal(s)
 */
export const GRAIL = (depth: number = 0) => gql`
	{
		grail {
      _id
			title
      done
      parents {
        title
        done
      }
      ${children(depth)}
		}
	}
`

export const GET_ALL_RELATIONS = gql`
	{
		links: goals {
      title
			link {
				from {
					Goal {
						title
					}
				}
				to {
					Goal {
						title
					}
				}
			}
		}
	}
`

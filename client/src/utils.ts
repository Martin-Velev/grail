import { GoalModel, GraphicalNode, LinkModel, GraphicalLink } from './models'
import React, { ReactElement } from 'react'

export const renderChild = (
	children: ReactElement | ReactElement[],
	props?: any
) => {
	return React.Children.map(children, (child: ReactElement) =>
		React.cloneElement(child, { ...props })
	)
}

export function elevatorize(root: GoalModel): GoalModel[][] {
	let result: GoalModel[][] = []

	function arrayize(goal: GoalModel, depth: number) {
		if (!goal || !goal.tasks || goal.tasks.length === 0) return

		if (result[depth] === undefined) {
			result[depth] = []
		}
		result[depth].push(goal)
		goal?.tasks?.forEach((g) => arrayize(g, depth + 1))
	}

	arrayize(root, 0)
	return result
}

export function goalToNode(g: GoalModel): GraphicalNode {
	return {
		id: g.title,
		// children: g.tasks?.map(goalToNode),
	}
}

export function relationsToNodeLinks(links: LinkModel[]): GraphicalLink[] {
	// WARNING: Magic! (not the good kind)
	// TODO: This could probably be done cleaner
	return links.reduce(
		(nodeLinks: GraphicalLink[], link: LinkModel) => [
			...nodeLinks,
			...link.link.to.map((toto) => {
				return { source: link.title, target: toto.Goal.title }
			}),
		],
		[]
	)
}

/**
 * [Goal1[Goal2[Goal3]]]  -> [Goal1, Goal2, Goal3]
 * @param goals - hirearchy of goals
 */
export function flatten(
	goals: GoalModel[],
	collapsedList: string[] = []
): GoalModel[] {
	let ret: GoalModel[] = []
	for (let i = 0; i < goals.length; i++) {
		const children = goals[i].tasks
		if (
			children &&
			children.length > 0 &&
			!collapsedList.includes(goals[i].title)
		) {
			ret = [...ret, goals[i], ...flatten(children, collapsedList)]
		} else {
			ret.push(goals[i])
		}
	}
	return ret
}

export function deepFilter(goals: GoalModel[], filter: Function): Array<any> {
	const ret: GoalModel[] = []
	for (let i = 0; i < goals.length; i++) {
		let g = goals[i]
		if (!filter(g)) continue
		if (g.tasks && g.tasks.length) {
			g.tasks = deepFilter(g.tasks, filter)
		}
		ret.push(g)
	}
	return ret
}

export const deepMap = (
	root: any,
	callback: Function,
	childAccessor: string = 'children',
	level: number = 0
): any => {
	const children = root[childAccessor]
	if (!children || children.length < 1) {
		return callback(root, level)
	}

	let result: any = { ...callback(root, level) }
	result[childAccessor] = root[childAccessor].map((element: any) =>
		deepMap(element, callback, childAccessor, level + 1)
	)
	return result
}

export function computeScrollTarget(
	allElementIDs: string[],
	elID: string,
	parentDivID: string,
	doc: Document = document
): number {
	// console.log(elID, parentDivID, allElementIDs)
	const el = doc.getElementById(elID)
	const containerEl = doc.getElementById(parentDivID)
	const computedTop = allElementIDs
		.map((id) => doc.getElementById(id)?.getBoundingClientRect().height)
		.reduce((total: any, height: any) => height + total, 0)

	if (containerEl && el) {
		const elHeight = el.getBoundingClientRect().height
		const computedBot = computedTop + elHeight
		const treeNavHeight = containerEl?.getBoundingClientRect().height
		const navTop = containerEl.scrollTop
		const navBot = navTop + treeNavHeight
		const cardInView = computedTop > navTop && computedBot < navBot
		if (!cardInView) {
			const isBelow = computedBot > navBot
			const isAbove = computedTop < navTop
			if (isBelow) {
				return computedTop - treeNavHeight + elHeight
			}
			if (isAbove) {
				return computedTop
			}
		}
	}
	return -1
}

export function trimString(str: string, length = 9) {
	return str.slice(0, length) + '...'
}

export function hasTasks(goal: GoalModel) {
	return goal.tasks && goal.tasks.length > 0
}
